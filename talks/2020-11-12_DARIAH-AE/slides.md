<!-- .slide: data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->

## Criteria for Recording and Categorizing Scholarly Digital Editions

  

Daniela Schulz, Herzog August Bibliothek Wolfenbüttel<!-- .element class="small" -->  
Simon Sendler, Goettingen State and University Library<!-- .element class="small" -->

[www.clariah.de](https://www.clariah.de/en/)<!-- .element class="small" -->
[@CLARIAHde](https://twitter.com/CLARIAHde)<!-- .element class="small" -->

Scholarly Primitives - DARIAH Annual Event 2020 (Session: Tools and Workflows)<!-- .element class="small" -->

---

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
### Structure

- On Data Sustainability in Scholarly Digital Editions
- Categorizing Scholarly Digital Editions
- Implementing a Survey
- Conclusion

|||

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
#### On Sustainability of Data in Scholarly Digital Editions

Expressivity of markup and interoperability can compete.

|||

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
##### Expressivity

- Scholars desire a native data model that is able to represent their source as precisely as possible
- Multiple markup options for one phenomenon

|||

> These Guidelines apply to texts in any natural language, of any date, in any literary genre or text type, <span class="inline fragment grow">without restriction on form or content</span>. They treat both continuous materials (‘running text’) and discontinuous materials such as dictionaries and linguistic corpora.

**Source:**<!-- .element class="small" --> [P5: Guidelines for Electronic Text Encoding and Interchange. iv. About These Guidelines.](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/AB.html)<!-- .element class="small" -->

|||

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
##### Interoperability

- As the complexity of a data model/markup increases, possibilities of interoperability decrease
- Interoperability is crucial for sustainability and re-use of research data

<span class="inline fragment shrink">N.B.: Interoperability ≠ Interchangability</span>

|||

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
##### A Compromise Between Expressivity and Interoperability

Provide research data in a **pivot format**.

- Identify and markup a core body of data
- Make data available to pipelines and tools
- Increased visibility of the edition through integration in corpora

---

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
#### Categorizing Scholarly Digital Editions

*Guiding Questions*:

- Are there features that regularly impact conversion complexity?
- Can we establish a workflow for these conversions?
- Can we support the community in converting SDEs?

|||

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
*Aim*:  
Provide a measure of comparability between different native formats of SDEs and aid scholars in converting data to a pivot formats.  

*Pivot Format of Choice*:  
[Base Format of the German Text Archive](http://www.deutschestextarchiv.de/doku/basisformat/)

|||

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
1. Editorial model
2. Markup
3. Discipline
4. Epoch of base material
5. Type/Transmission of base material
6. Language of base material
7. Number of documents to be encoded

|||

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
##### 1. Editorial model

- Historical-Critical Edition
- (Hyper-)Diplomatic Edition
- Genetic Edition
- Reader’s Edition
- Transcription
- (partial) Translation

|||

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
1. Editorial model
2. <span class="inline fragment grow" data-fragment-index="1"><span class="inline fragment shrink" data-fragment-index="2">Markup</span></span>
3. <span class="inline fragment grow" data-fragment-index="2"><span class="inline fragment shrink" data-fragment-index="3">Discipline</span></span>
4. <span class="inline fragment grow" data-fragment-index="2"><span class="inline fragment shrink" data-fragment-index="3">Epoch of base material</span></span>
5. <span class="inline fragment grow" data-fragment-index="3"><span class="inline fragment shrink" data-fragment-index="4">Type/Transmission of base material</span></span>
6. <span class="inline fragment grow" data-fragment-index="3"><span class="inline fragment shrink" data-fragment-index="4">Language of base material</span></span>
7. <span class="inline fragment grow" data-fragment-index="4"><span class="inline fragment shrink" data-fragment-index="5">Number of documents to be encoded</span></span>

---

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
##### Implementing a Survey

- Estimate the complexity of transformation into pivot format
- Provide basic recommendations regarding transformation
- Cross-reference projects of similar scope

|||

![EdMa, Editorial Models by Discipline](img/editorial_models_by_Discipline.png)

|||

![EdMa, Source Material by Discipline](img/Source_Material_Use_by_Discipline.png)

---

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
##### Conclusion

- Provision of data in a pivot format as well as the native format as a compromise between expressiveness and interoperability
- Create recommendations for "pivot-friendly native formats"

---

<!-- .slide: class="leftAligned" data-background="url('img/2019-06-03_CLARIAH-DE_.png') 95% 2%/20% no-repeat" -->
**References**

- <!-- .element class="small" -->Syd Bauman: “Interchange vs. Interoperability.” In: Proceedings of Balisage: The Markup Conference 2011. Balisage Series on Markup Technologies, Volume 7 (2011). https://doi.org/10.4242/BalisageVol7.Bauman01
- <!-- .element class="small" -->Martin Holmes: "Whatever happened to interchange?" In: Digital Scholarship in the Humanities, Volume 32, Issue suppl_1, April 2017, Pages i63–i68. https://doi.org/10.1093/llc/fqw048
- <!-- .element class="small" -->Daniela Schulz: Überlegungen zu Harmonisierung und Standardisierung im Rahmen des Projekts CLARIAH-DE – oder: Aus der „Suppenküche“ von Arbeitspaket 1: Forschungsdaten, Standards und Verfahren. In: DHd-Blog, 10.07.2020. https://dhd-blog.org/?p=14064 (11.10.2020).



- <!-- .element class="small" -->FAIR Principles: https://www.force11.org/group/fairgroup/fairprinciples.
- <!-- .element class="small" -->P5: Guidelines for Electronic Text Encoding and Interchange. iv. About These Guidelines: https://www.tei-c.org/release/doc/tei-p5-doc/en/html/AB.html (11.10.2020).

*Graphics courtesy of Susann Schwaß, Wolfenbüttel*<!-- .element class="small" -->

---

![Logo CLARIAH-DE, BMBF, Creative Commons (cc by nc sa)](img/2019-08-01_Zusammenstellung_Logo_Twitter_BMBF.png)<!-- .element class="img-small" -->

**Criteria for Recording and Categorizing Scholarly Digital Editions**

  

Daniela Schulz, Herzog August Bibliothek Wolfenbüttel<!-- .element class="small" -->  
Simon Sendler, Goettingen State and University Library<!-- .element class="small" -->

[www.clariah.de](https://www.clariah.de/en/)<!-- .element class="small" -->
[@CLARIAHde](https://twitter.com/CLARIAHde) <!-- .element class="small" -->

Scholarly Primitives - DARIAH Annual Event 2020 (Session: Tools and Workflows)<!-- .element class="small" -->
