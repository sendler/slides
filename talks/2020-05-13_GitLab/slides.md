## Einführung: Gitlab

---

## Basics

--

- Group/Project/Repository
- Cloning/Forking
- Commiting/Pushing
- Branching

--

- __Group/Project/Repository__
- __Cloning/Forking__
- Commiting/Pushing
- Branching

--

- Group/Project/Repository
- Cloning/Forking
- __Commiting/Pushing__
    - commit messages mit Großbuchstaben anfangen und im Imperativ schreiben
    - sollen die Intention ausdrücken (nicht: "updated readme.md" sondern: "Update license information in readme.md")
- Branching

--

- Group/Project/Repository
- Cloning/Forking
- Commiting/Pushing
- __Branching__

---

## Issue Management

- Issue Title: sollte eher das _Ziel_ ausdrücken als das Problem
- Issue Features: Assignee, Labels, Due Date
- Milestones, denen Issues zugewiesen werden können
- Boards als Überblick

--

<!-- .slide: data-background-iframe="https://gitlab.gwdg.de/sendler/slides/-/issues/1" -->

--

Grundsätzlich kann in den meisten Textboxen Markdown verwendet werden.
<small>Siehe: https://gitlab.gwdg.de/help/user/markdown.md</small>
- praktisch: Taskliste ```- [ ] Text```
- weitere Shortcuts
    - \# Issues
    - ! Merge Requests
    - / Shortcuts
    - : Emojis
    - @ User Address (kein richtiger Ping, da keine Nachricht an User, aber es tauch in der To Do-Liste auf)

---

## Schreiben und Präsentieren mit GitLab

__Texte schreiben__

- in Markdown schreiben
- dann mit [Pandoc](https://pandoc.org/) konvertieren

--

__Präsentationen__

- GitLab CI + GL Pages + reveal.js
- Repo mit generic template hier forken: https://gitlab.gwdg.de/mgoebel/slides

--

1. Präsentationen ebenfalls in Markdown schreiben (slides.md)
  - auch wichtig: die korrekten Metadaten in index.html (Title, Name, Description)
2. per GitLab Pages die Präsentation anzeigen: https://USERNAME.pages.gwdg.de/slides/talks/generic-markdown/

--

- Pages wird pro Commit aktualisiert, das dauert manchmal ein, zwei Minuten
- Anpassungen im CSS möglich
    - direkt in slides.md: \<!-- .slide: background="blue" -->
    - in CSS-file
        - Klasse zuweisen: \<!-- .element class="CLASS" -->
        - ID zuweisen: \<!-- .element id="ID" -->
- auch schön zu Demonstrationszwecken: iframes

<small>Zum nochmal in Ruhe nachlesen und weitere Info: https://lab.sub.uni-goettingen.de/markdown-reveal-pages.html</small>

---

## Entwickeln im GitLab

--

### git flow

- Branchingmodell für git
  - semantisch getrennte branches
- [Vincent Driessen: “A successful Git branching model”](https://nvie.com/posts/a-successful-git-branching-model/)

--

![git flow](img/git-model@2x.png)<!-- element: height="600" -->

<small>https://nvie.com/img/git-model@2x.png</small>

--

<!-- .slide: data-background-iframe="https://danielkummer.github.io/git-flow-cheatsheet/" -->

--

### Warum git flow?

Modularer Aufbau ermöglicht...

- weniger Merge-Konflikte
- übersichtliche Repo-History
- Code Review
- (teil-)automatische Erstellung der changelogs

Natürlich muss die GitLab CI entsprechend konfiguriert sein.

--

#### Issue-Driven Development

- am Anfang steht immer ein Issue
    - Nachvollziehbarkeit und Planparkeit im Projektes
- nie mehr als ein Branch pro Issue
- Branches als feature oder bugfix

--

```bash
git flow feature start "#01-short-issue-description"
```

Oder im GitLab erstellen.

--

- der erste Commit sowie der merge request sollten ebenfalls das entsprechende Issue referenzieren
- MR mit "closes #n" oder "fixes #n" schließen automatisch das referenzierte Issue, wenn sie angenommen werden
    - MR mit `WIP:` oder `[WIP]` (work in progress) können nicht angenommen werden → nützlich, wenn features diskutiert werden sollen

--

- beim merge von feature branches können die source branches entfernt werden
    - macht den Graph übersichtlicher
    - sollte das Issue doch noch einmal wieder eröffnet werden müssen, kann man den gleichen branch name verwenden
    - projektabhängig geregelt

--

Nach gestelltem MR:
1. Code Review
1. evtl. Revisionen und erneute Review
1. Merge

--

![git flow](img/git-model@2x.png) <!-- element: height="600" -->
<small>https://nvie.com/img/git-model@2x.png</small>

---

Fragen?

--

Danke!
