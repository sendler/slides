![Logo CLARIAH-DE](img/2019-06-03_CLARIAH-DE_.png)

Simon Sendler, Goettingen State and University Library<!-- .element class="small" -->

[www.clariah.de](https://www.clariah.de/en/)<!-- .element class="small" -->
[@CLARIAHde](https://twitter.com/CLARIAHde)<!-- .element class="small" -->

---

### What is a digital research infrastructure?

Notes:

Collect input and refer back to it at the end.

---

### CLARIAH-DE: Benchmarks

* Funding: [Federal Ministry of Education and Research](https://www.bmbf.de/en/index.html)

* Project Duration: 01.03.2019 – 03.31.2021

* Aims
  * <!-- .element class="small" -->Harmonize and integrate existing offers of [DARIAH-DE](https://de.dariah.eu/en/web/guest/home) and [CLARIN-D](https://www.clarin-d.net/en/)
  * <!-- .element class="small" -->Contribution to [National Research Data Infrastructure](https://www.nfdi.de/en-gb)

Notes: 

Participating are twelve executive partners and thirteen additional institutions. This includes technical infrastructure, repositories, and services.

---

<!-- .slide: class="leftAligned" -->
### Work Packages

* WP 1 – Research Data, Standards and Procedures
* WP 2 – Tools and Virtual Research Environments
* WP 3 – Skill Training and Promotion of Junior Researchers
* WP 4 – Technical Integration and Coordination of Technical Developments
* WP 5 – Community-Engagement: Outreach/ Dissemination and Liaison
* WP 6 – Organisational Infrastructure/Administrative Offices

--

<!-- .slide: class="leftAligned" -->
#### WP1 – Research Data, Standards and Procedures

* Integration of digital research data from projects and community
* Cooperation with standards bodies

#### WP 2 – Tools and Virtual Research Environments

* Integration of tools and services
* Adaptation of the [Language Resource Switchboard](https://www.clarin.eu/content/language-resource-switchboard)

--

<!-- .slide: class="leftAligned" -->
#### WP 3 – Skill Training and Promotion of Junior Researchers

* Creation and compilation of teaching material
* Courses teaching necessary technical skills and "data literacy"

#### WP 4 – Technical Integration and Coordination of Technical Developments

* Integration of technical infrastructure
* Coordination of further technical development

--

<!-- .slide: class="leftAligned" -->
#### WP 5 – Community-Engagement: Outreach/ Dissemination and Liaison

* Integration of web sites
* Creation of information material

#### WP 6 – Organisational Infrastructure/Administrative Offices

* Establishment and running of a joint Helpdesk
* Administration and internal/external coordination

--

<!-- .slide: class="leftAligned" -->
### Work Packages

* WP 1 – Research Data, Standards and Procedures
* WP 2 – Tools and Virtual Research Environments
* WP 3 – Skill Training and Promotion of Junior Researchers
* WP 4 – Technical Integration and Coordination of Technical Developments
* WP 5 – Community-Engagement: Outreach/ Dissemination and Liaison
* WP 6 – Organisational Infrastructure/Administrative Offices

Notes:

* Refer back to input from the beginning, if any.
* CLARIAH-DE sees itself as an open infrastructure for researchers, independent of their institution, providing access to any know-how and tools they might need to facilitate their research.
* is fundamentally an open project that wants to disseminate knowledge and tools, not centralize them --> this explains the appearance of many of the tools offered: they originate from specific research projects and are adapted for general use during integration into the infrastructure
* but there are genuine services and tools, such as the AAI or the wiki
* aspects regarding the dissemination of tools also refer to research data: we not only want to facilitate research by providing repositories, but also make them available in a standardized format, for reusability and sustainability

---

### What can a research infrastructure such as CLARIAH-DE do for your institution?

Notes:

What do you think CLARIAH could do for you? What do you *wish* it could?

---

### Further Resources

* [CLARIAH-DE Leaflet](https://www.clariah.de/images/pdfs/2020-04-en-Flyer-Faltblatt-CLARIAH-DE.pdf)
* [CLARIAH-DE Overview: Support & Guidance](https://www.clariah.de/en/consulting-training)
* [CLARIAH-DE Helpdesk Contact Form](https://www.clariah.de/en/support-helpdesk) (also: support@clariah.de)
* [CLARIAH-DE Services](https://www.clariah.de/en/about-us/service-list)

Notes:

Please be aware that the CLARIAH-DE homepage is still in development, so the exact content of the pages might change slightly during the remainder of the project. Still, the links will remain viable.

---

![Logo CLARIAH-DE, BMBF, Creative Commons (cc by nc sa)](img/2019-08-01_Zusammenstellung_Logo_Twitter_BMBF.png)<!-- .element class="huge-img" -->

Simon Sendler, Goettingen State and University Library<!-- .element class="small" -->

[www.clariah.de](https://www.clariah.de/en/)<!-- .element class="small" -->
[@CLARIAHde](https://twitter.com/CLARIAHde) <!-- .element class="small" -->
